from pymongo import MongoClient
from bson.objectid import ObjectId
from tqdm import tqdm
import csv
import os

# Create a connection to the database server
client = MongoClient('jobsreports01.prod.phenom.local:27017',
                     username='ajay.chinni',
                     password='xKYLqbJ83yWdpE4y',
                     authSource='JobpullDB',
                     authMechanism='SCRAM-SHA-1')

database = client["JobpullDB"]
collection = database["statusTrack"]
data = []

# opening csv and extracting the last id from csv
path = os.getcwd()

with open(path+"/Whole_StatusTrack.csv",'r') as scraper:
    reader = csv.reader(scraper)
    for row in reader:
        data.append(row[0])
LastId = data[-1]

# updating new latest id got from database
where = {"_id": {"$gt":ObjectId(str(LastId))}}

projection ={ "_id": 1, "refNum": 1, "timeStamp": 1, "uuid": 1, "atsJobsCount": 1,"scrapedJobsCount": 1,
              "newJobsCount": 1, "updatedJobsCount": 1, "deletedJobsCount": 1}

cursor = collection.find(where, projection)

try:
    for doc in tqdm(cursor):
        with open(path+"/Whole_StatusTrack.csv",'a',newline='' ) as file:
            writer = csv.writer(file)
            writer.writerow([doc.get('_id'),doc.get('refNum'),doc.get('timeStamp'),doc.get('uuid'),doc.get('atsJobsCount'),doc.get('scrapedJobsCount'),doc.get('newJobsCount'),doc.get('updatedJobsCount'),doc.get('deletedJobsCount')])
finally:
    client.close()
